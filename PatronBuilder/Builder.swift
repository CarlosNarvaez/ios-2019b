import  XCTest 

/// La interfaz del generador especifica métodos para crear las diferentes partes de los objetos del producto. 
Protocol  Builder  { 

    func  producePartA ( ) 
    func  producePartB ( ) 
     
} 

/// Las clases de Concrete Builder permite implementaciones específicas 
class  ConcreteBuilder1 :  Builder  {

    /// Creamos un producto en blanco, que 
    private  var  product  =  Product1 ( ) 

    func  reset ( )  { 
        product  =  Product1 ( ) 
    } 

    /// Todos los pasos de producción funcionan con la misma instancia del producto. 
    func  producePartA ( )  { 
        product .add ( parte :  "PartA1" ) 
    } 

    func  producePartB ( )  { 
        product .add (parte :  "PartB1" ) 
    } 

    //Recupero el producto que solicité y reseteo para un nuevo 
    func  retrieveProduct ( )  - >  Product1  { 
        let  result  =  self .product 
        reset ( ) 
        return  result 
    } 
} 



    /// El Director puede construir varias variaciones de productos usando los mismos 
    /// pasos de construcción. 
    func  buildMinimalViableProduct ( )  { 
        constructor ? .producePartA ( ) 
    } 

    func  buildFullFeaturedProduct ( )  { 
        constructor ? .producePartA ( ) 
        constructor ? .producePartB ( ) 
     
    } 
} 

 
class  Product1  { 

    private  var  parts  =  [ String ] ( ) 

    func  add ( part :  String )  { 
        self .parts .append ( part ) 
    } 

    func  listParts ( )  - >  String  { 
        return  "Piezas del producto:" +  partes .joined ( separador :  "," )  +  "\ n" 
    } 
} 

 
clase  Cliente  { 
 
        
        print ( "Producto básico estándar:" ) 
        director .buildMinimalViableProduct ( ) 
        print ( constructor .retrieveProduct ( ) .listParts ( ) ) 

    
        print ( "Producto personalizado:") 
        builder .producePartA ( ) 
        builder .producePartC ( ) 
        print ( builder .retrieveProduct ( ) .listParts ( ) ) 
    } 
    // ... 
} 

class  BuilderConceptual :  XCTestCase  { 

    func  testBuilderConceptual ( )  { 
        var  director  =  Director ( ) ; 
        Cliente .someClientCode ( director :  director) 
    } 
}


