import UIKit

var str = "Hello, playground"
var number:Double = 5

number = 8

let pi = 3.14

for _ in 1...5{
    print("Hello")
}



for i in 1...5 where i%2 == 0 {
    print("Hello")
}


for i in stride(from: 1, to: 10, by: 2){
    print("Hello : \(i)")
}

var k = 0
while k < 10 {
    print (k)
    k += 1
}

var j = 0
var i = 0
while i < 10, j > 3 {
("i:\(i) - j:\(j)")
    i += 1
    j -= 1

}


var numbers = [1,2,3,4,5,6,7]
numbers.append(8)
numbers += [9]


var things:[Any]

let 🚖 = "Taxi"

let año = 2019

let evenNumbers = numbers.filter {number in
    number % 2 == 0
}

let even = numbers.filter { $0 % 2 == 0 }

let evenn = numbers
    .filter { $0 % 2 == 0 }
    .map{$0 * $0}
    .map { Double($0) * 0.15 }

even


//estas 3 funciones hacen lo mismo

let sum = numbers.reduce(0) { (result, next) in
return result + next
}

sum

let sum2 = numbers.reduce(0) { $0 + $1 }
sum2

let sum3 = numbers.reduce(0, +)
sum3
//


let dictionary = [
    "name": "Carlos",
    "lastname": "Narvaez",
    "jobTitle": "Student"
]
numbers[0]
dictionary["name"]




let temperature = 16
switch temperature {
case 1...8:
    print("It`s cold! ❄️")
case 9...12:
    print("Not to cold! 💨")
case 13...17:
    print("Just about right ☁️")
default:
    print("------")
}


//FUNCIONES

func myFunction() {
    print("Hey !")
}
myFunction()


func square(number: Int) -> Int { // el -> me dice lo q va a retornar
    return number * number
}

square(number: 7)

func sendMessage (nombre: String, mail: String){
    print ("Hola:" + nombre + mail)
}
sendMessage(nombre: "carlos", mail: "cnat")



func send1 (message: String, to recipient: String){
    print (recipient)
}

send1(message: "Hey dude!", to: "cnat@sas.com")

func square2(_ number: Int) -> Int { // el _ sirve para q al instanciar la funcion solo se le ponga el numero mas no la variable y el tipo
    return number * number
}
square2(3)

if temperature > 1, temperature % 2 == 0 { // la coma significa "y"
    
}else{
    
}

func sSun (n1: Int, n2: Int, n3: Int = 0) -> Int {
    return n1 + n2 + n3
}
sSun(n1: 1, n2: 6, n3: 4)

 //FUNCIONES DENTRO DE OTRAS

func mult (n1: Int, n2: Int) -> Int {
    return n1 * n2
}

func takes (n1: Int, n2: Int) -> Int {
    return n1 - n2
}

func opp (n1: Int, n2: Int, operation: ((Int, Int) -> Int)) -> Int {
    return operation(n1, n2)
}
opp(n1: 4, n2: 5, operation: takes(n1:n2:))
opp(n1: 3, n2: 2, operation: mult(n1:n2:))


func specialSum<T: Numeric> (n1: T, n2: T) ->T {
    return n1 + n2
}

let r1 = specialSum(n1: 4.355555555555, n2: 5.95555555555)
r1

let r2 = specialSum(n1: 4, n2: 3)
r2



func recursive(a: Int, sum: Int) -> Int {
    if a == 0 {
        return sum
    } else {
        return recursive (a: a - 1, sum: sum + a)
    }
}

recursive(a: 10, sum: 0)


// TUPLAS
let coord2D = (x: 4, y: 5)
let coord3D = (x: 4.1, y: 6, z: 8.3)

// Asi hago que una funcion saque el area y perimetro de un cuadrado
func badFunction (lado: Int) -> (perimetro: Int, area: Int) {
    return (lado * 4, lado * lado)
}

badFunction(lado: 5)
badFunction(lado: 5).area
badFunction(lado: 5).perimetro


// Classes & Structs

//Paso por valor: STRCTS utiliza menos memoria que una clase, es INMUTABLE
//Paso por rederencia: CLASE

struct Car {
    var maxSpeed: Int
    var brand: String
    
    mutating func incrementMaxSpeed() {
        maxSpeed += 10
    }
}
var carA = Car (maxSpeed: 200, brand: "VW")
carA.brand = "Audi"


class Person {
    let name: String
    let lastName: String
    let nId: String
    
    init(name: String, lastName: String, nId: String){
        self.name = name
        self.lastName = lastName
        self.nId = nId
    }
}

let PersonA = Person(name: "Carlos", lastName: "Narvaez", nId: "1725457654")

protocol Friendable {
    var friend : Person? {get set}
    
    func addFriendable (friend: Person)
    }

// EJEMPLPO DE HERENCIA
class Employee: Person, Friendable {
    
    
    let jobTitle: String
    var salary: Double?
    var friend: Person?
    
    init(
        name: String,
        lastName: String,
        nId: String,
        jobTitle: String){
    
    self.jobTitle = jobTitle
    super.init(name: name, lastName: lastName, nId: nId)
    
}
    
    func addFriendable(friend: Person) {
        self.friend = friend
        
    }
    
    func singleFunction(var1: Int){
        
    }
    
    func singleFunction (var1: String){
        
    }
    
    func doSomething(){
        print("I´m a Employee")
    }
    
    static func printSomething(){
        print("Static called")
    }
    }

//POLIMORFISMO
func polimorphic (person: Person){
    print(person.name)
}

let employee = Employee(name: "Carlos", lastName: "Narváez", nId: "1725459026", jobTitle: "Student")
polimorphic(person: employee)

var myOptional: Int?
print(myOptional)

myOptional = 10
print(myOptional)

func somethingCool (number: Int){
    print("number is: \(number)")
}

somethingCool(number: myOptional ?? 0) // Le digo que si le pasa un null le ponga 0

func othersomething (number: Int?){
    if let noNumber = number, noNumber % 2 == 0 {
    print("number is: \(noNumber)")
        noNumber
    }
    
}

myOptional = nil
othersomething(number: myOptional)

func moreOptionals(number: Int?) {
    guard let noNumber = number else {
        print("NULLL!!!")
        return
    }
    print(noNumber)
}
moreOptionals(number: myOptional)

myOptional = 20

print(myOptional)
print(myOptional!)



