import Foundation
protocol AudioHandling {
  func handle(audio: Audio) -> Audio
}

class AudioEncryptor: AudioHandling {
  func handle(audio: Audio) -> Audio {
    // Encrypt and return Audio
  }
}

class PlainAudioHandler: AudioHandling {
  func handle(audio: Audio) -> Audio {
    // return default audio
  }
}

class CellPhone: AudioCommunicationsDevice {
  
  let audioHandler: AudioHandling
  
  init(audioHandler: AudioHandling) {
    super.init()
    
    self.audioHandler = audioHandler
  }
  
  override func sendAudio() {
    // Use audioHandler(audio:) to prepare audio, then send audio
  }
}

class WalkieTalkie: AudioCommunicationsDevice {
  
  let audioHandler: AudioHandling
  
  init(audioHandler: AudioHandling) {
    super.init()
    
    self.audioHandler = audioHandler
  }
  
  override func sendAudio() {
    // Use audioHandler(audio:) to prepare audio, then send audio
  }
}

let audioHandler = AudioEncryptor()
let walkieTalkie = WalkieTalkie(audioHandler: audioHandler)

walkieTalkie.sendAudio()