//
//  RideViewController.swift
//  UBBER3
//
//  Created by CNAT on 12/17/19.
//  Copyright © 2019 CNAT. All rights reserved.
//

import UIKit
import MapKit
import Firebase

//20-12-2019

class RideViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    
    
    @IBOutlet weak var mapview: MKMapView!
    let locationManager = CLLocationManager()
    let firestore = Firestore.firestore() //inicializo
    
    var annotattion: [String: (MKPointAnnotation, UIColor)] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        
        
        locationManager.requestWhenInUseAuthorization()
        //locationManager.desiredAccuracy=kCLLocationAccuracyBest
        locationManager.desiredAccuracy=kCLLocationAccuracyNearestTenMeters
        locationManager.distanceFilter=kCLDistanceFilterNone //se puede ver la distancia a la que se va a encontrar los taxis
        locationManager.startUpdatingLocation() //muestra el puntito donde esta parado
        locationManager.delegate = self
        mapview.showsUserLocation = true
        centerMapIn(location: locationManager.location)
        getAllUsers()
        
        mapview.delegate = self // Para que cada usuario tenga su color y MKMapView en el inicio
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //centerMapIn(location: locations[locations.count - 1])
        let lastLocation = locations[locations.count - 1].coordinate
        let lat = lastLocation.latitude.description
        let long = lastLocation.longitude.description
        firestore.collection("location").document(String(Auth.auth().currentUser!.uid)).setData(
            [
                "coordinates":[lat, long],
                "color": [40.0/255, 19.0/255, 19.0/255]
            ]
            
        )
    }
    
    func centerMapIn(location: CLLocation?) {
        
        guard let location = location else {
            return
        }
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 2000, longitudinalMeters: 2000)
        mapview.setRegion(coordinateRegion, animated: true)
        
        
    }
    
    //mostrar los carros en el mapa
        
        func getAllUsers() {
            firestore
                .collection("location")
                .getDocuments { (snapshot, error) in
                    if error != nil {
                    print(error)
                        return
                    }
            
            
            for document in snapshot!.documents {
                self.showUser(id: document.documentID)
            }
            }
    }
            
            
            func showUser (id: String){
                
                if id == String(Auth.auth().currentUser!.uid){
                    return
                }
                
                //let annotation = MKPointAnnotation()
                let annotation = UberAnnotation()
                
                
                firestore
                .collection("location")
                .document(id)
                .addSnapshotListener { (snapshot, error) in
                    if error != nil {
                        print(error)
                        return
                    }
                
                let colorData = snapshot?.get("color") as! Array<Double>
                let rgbColor = colorData.map {CGFloat($0) }
                annotation.userColor = UIColor(red : rgbColor [0], green: rgbColor [1], blue: rgbColor [2], alpha: 1)
                    annotation.userId = id
                    
                    self.mapview.addAnnotation(annotation)
                }
                
                //mapview.addAnnotation(annotation)
                
                firestore
                    .collection("location")
                    .document(id)
                    .addSnapshotListener { (snapshot, error) in
                        if error != nil {
                            print(error)
                            return
                        }
                        let data = snapshot?.get("coordinates") as! Array<String>
                        let coordinates = data.map { Double ($0)! }
                        let lat = coordinates [0]
                        let long = coordinates [1]
                        
                        let annotationCoordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                //print(snapshot)
                        annotation.coordinate = annotationCoordinate
                        
                        //para imprimir el color de usuario
                        
                        //let colorData = snapshot?.get("coordinates") as! Array<Double>
                        //let rgbColor = colorData.map {CGFloat($0) }
                       // annotation.userColor = UIColor(red : rgbColor [0], green: rgbColor [1], blue: rgbColor [2], alpha: 1)
            }
            }
        // MARK: - Navigation

        // In a storyboard-based application, you will often want to do a little preparation before navigation
      
        
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
      if annotation is MKUserLocation{
        return nil;
      }else{
        
        let customAnnotation = annotation as! UberAnnotation // color del usuarioo
        //print(customAnnotation.userId)
        //print(customAnnotation.userColor)
        let pinIdent = "Pin";
        //var pinView: MKPinAnnotationView;
        var pinView: UberAnnotationView;

        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: pinIdent) as? UberAnnotationView {
          dequeuedView.annotation = customAnnotation;
          pinView = dequeuedView;
        }else{
            pinView = UberAnnotationView(annotation: customAnnotation, reuseIdentifier: pinIdent, image: nil ,color: customAnnotation.userColor);
        }
        
       // pinView.pinTintColor = customAnnotation.userColor
        return pinView;
      }
    }
    
    
    @IBAction func centerLocationPressed(_ sender: Any) {
        centerMapIn(location: locationManager.location)
    }
    
    
    
}
