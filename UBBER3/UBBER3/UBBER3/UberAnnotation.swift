//
//  UberAnnotationView.swift
//  UBBER3
//
//  Created by CNAT on 1/7/20.
//  Copyright © 2020 CNAT. All rights reserved.
//

import Foundation
import MapKit

class UberAnnotation: MKPointAnnotation {
    var userId: String?
    var userColor: UIColor?
}
