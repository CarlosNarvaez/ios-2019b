//
//  ViewController.swift
//  UBBER3
//
//  Created by CNAT on 11/26/19.
//  Copyright © 2019 CNAT. All rights reserved.
//

import UIKit
import FirebaseAuth //Añadido


class ViewController: UIViewController {
    
//AQUI VAN LOS TEXTFIELD
    
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
//FIN DE LOS ELEMENTOS CREADOS
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        if let _ = Auth.auth().currentUser{
            performSegue(withIdentifier: "homeTOlogin", sender: self)
        }
    }
        
        
    
    
//CAMPO DE LOS BOTONES
    

    @IBAction func loginButton(_ sender: Any) {
        
        guard let email = emailTextField.text,
              let password = passwordTextField.text else {
                      return
                  }
                  Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
                          //print("RESULT: \(result)")
                      //print("ERROR: \(error)")
                      
                      if error != nil {
                          self.presentAlertWith(title: "Error", message: error?.localizedDescription ?? "Ups! An error ocurred")
                      } else{
                          self.performSegue(withIdentifier: "homeTOlogin", sender: self) //union entre pantallas
                      }
                  }
        
    }
    

    private func presentAlertWith(title: String, message: String){
             let alertControlller = UIAlertController(title: title, message: message, preferredStyle: .alert)
             
             let okAlertAcction = UIAlertAction(title: "OK", style: .default) {
                 _ in
                 self.emailTextField.text = ""
                 self.passwordTextField.text = ""
                 
                 self.emailTextField.becomeFirstResponder()
                 
             }
             
             alertControlller.addAction(okAlertAcction)
             
             present(alertControlller, animated: true, completion: nil)
         }
    
    
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {//funcion que se ejecuta cada vez que tocamos la pantalla
             //emaiTextField.resignFirstResponder()
             //passwordTextField.resignFirstResponder()
             
             view.endEditing(true)//todo lo que este editandose se va a cerrar
         }
    
    
    @IBAction func registerButton(_ sender: Any) {
    }
    
    @IBAction func cancelButton(_ sender: Any) {
    }
    
    
}

