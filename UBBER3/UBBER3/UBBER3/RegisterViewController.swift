//
//  RegisterViewController.swift
//  UBBER3
//
//  Created by CNAT on 11/26/19.
//  Copyright © 2019 CNAT. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseStorage
import FirebaseAuth

class RegisterViewController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate {
    
    //MARK: Attributes
    
    @IBOutlet weak var firstnameTextField: UITextField!
    @IBOutlet weak var lastnameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var pictureImageView: UIImageView!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    let birtDatePicker = UIDatePicker()
    var imagePicker = UIImagePickerController()
    
    //MARK: -Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateTextField.delegate = self
        birtDatePicker.datePickerMode = .date
        birtDatePicker.addTarget(self, action: #selector(dateValueChanged(_:)), for: .valueChanged)
        
        activityView.hidesWhenStopped = true
        activityView.isHidden = true

        // Do any additional setup after loading the view.
        
    }
    
    
    //MARK: -Logic
    
    @IBAction func pictureButtonPressed(_ sender: Any) {
        let actionSheet = UIAlertController(title: "Add Image", message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default){
            (_)in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera){
            
            
                     self.imagePicker.delegate = self
                     self.imagePicker.sourceType = .camera
                     self.imagePicker.allowsEditing = true
                     self.present(self.imagePicker, animated: true, completion: nil)
            
        }
        
    }
        
        
        let libraryAction = UIAlertAction(title: "Library", style: .default)
        { (_) in
               if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum)
               {
        
        
                 self.imagePicker.delegate = self
                 self.imagePicker.sourceType = .savedPhotosAlbum
                 self.imagePicker.allowsEditing = true
        
        
                 self.present(self.imagePicker, animated: true, completion: nil)
               }
        }
             
             actionSheet.addAction(cameraAction)
             actionSheet.addAction(libraryAction)
             
             present(actionSheet, animated: true, completion: nil)
           }
           
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
             pictureImageView.image = info[.editedImage] as! UIImage
             picker.dismiss(animated: true, completion: nil)
    }
        
    func textFieldDidBeginEditing(_ textField: UITextField) {
             if textField.tag == dateTextField.tag {
               textField.inputView = birtDatePicker
             }
           }
    //deletavaluecheage
    
    
           
        
    
    
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        
        activityView.isHidden = false
        activityView.startAnimating()
    
        createUser { userUID in
            self.uploadImage(userUID : userUID){ imageURL in
                self.saveToFirestore(imageURL: imageURL, userUID: userUID){
                    
                    self.activityView.stopAnimating()
                    //go to next screen
                    
                    self.performSegue(withIdentifier: "successloggin", sender: self)
                }
            }
        }
    
    }
        
    
    
    func uploadImage(userUID: String, completionHandler: @escaping (String) -> ()) {
             let storage = Storage.storage()
             let ref = storage.reference()
             let profileImages = ref.child("users")
             let userPicture = profileImages.child("\(userUID).jpg")
             let data = pictureImageView.image?.jpegData(compressionQuality: 0.5)!
             
             let _ = userPicture.putData(data!, metadata: nil){ (metadata, error) in
                
                
                if error != nil{
                    self.showAlert(title: "ERROR", message: error?.localizedDescription ?? "ERROR")
                }
                
                 guard let _ = metadata else {
                     return
                 }
         
                 userPicture.downloadURL { (url, error) in
                    
                     if error != nil { self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
                    }
                    
                     guard let downloadURL = url else {
                         return
                     }
                     completionHandler(downloadURL.absoluteString)
                 }
             }
       }
    
    
    
      func saveToFirestore(imageURL: String,
                           userUID: String,
                           completionHandler: @escaping ()->() ) {
    
         let firestore = Firestore.firestore()
         firestore.collection("users").document(userUID).setData(
         [
             "name" : self.firstnameTextField.text ?? "",
             "lastName" : self.lastnameTextField.text ?? "",
             "email" : self.emailTextField.text ?? "",
             "bithdate" : self.dateTextField.text ?? "",
             "phone" : self.phoneTextField.text ?? "",
             "imageURL" : "\(imageURL)"
             
         ]){(error) in
            
            
            if error != nil { self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
            }
            
            
             if error == nil {
                 completionHandler()
             }
         }
        }
          
            func createUser(completionHandler: @escaping (String)->()) {
          
              let auth = Auth.auth()
              auth.createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (authInfo, error) in
                
                if error != nil { self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
                }
                
                
                if error == nil {
                  completionHandler(authInfo!.user.uid)
                }
               }
          
             }
    
    func showAlert(title: String, message: String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertController.addAction(okAlertAction)
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonPresed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func dateValueChanged(_ sender: UIDatePicker) {
        let format = DateFormatter()
        format.dateFormat = "dd - MM - YYY"
        dateTextField.text = format.string(from: birtDatePicker.date)
    }

    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
